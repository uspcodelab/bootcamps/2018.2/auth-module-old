const fetch = require("node-fetch");
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const APP_SECRET = "FUCKING_WORK_GOD_DAMMIT";

const getUserUID = context => {
  const authorization = context.request.get("authorization");

  if (authorization) {
    const token = authorization.split(" ")[1];
    const { userId } = jwt.verify(token, APP_SECRET);
    return userId;
  }

  throw new Error("Not authenticated");
};

const resolvers = {
  Query: {
    getUserInfo: async (_, args, context, info) => {
      const user = await context.prisma.query.user({ where: { id: args.id } });

      return user;
    },

    getHackathons: async (_, args, context, info) => {
      let filter = {};
      if (args) filter = { where: { owner_id: args.owner_id } };

      const hackathons = await context.prisma.query.hackathons(filter);
      return hackathons;
    }
  },

  Mutation: {
    signup: async (_, args, context, info) => {
      const password = await bcrypt.hash(args.password, 10);

      const user = await context.prisma.mutation.createUser({
        data: {
          email: args.email,
          name: args.name,
          password
        }
      });

      return {
        token: jwt.sign({ userId: user.id }, APP_SECRET),
        user
      };
    },

    login: async (_, args, context, info) => {
      const user = await context.prisma.query.user({
        where: { email: args.email }
      });

      if (!user) throw new Error("No such user found");

      const valid = await bcrypt.compare(args.password, user.password);

      if (!valid) throw new Error("Wrong password");

      return {
        token: jwt.sign({ userId: user.id }, APP_SECRET),
        user
      };
    },

    logout: async (_, args, context, info) => {
      return "Bearer ";
    },

    validate: async (_, args, context, info) => {
      return getUserUID(context);
    }
  }
};

export default resolvers;
